
import config  from "./config.js"
import express from "express"

import getCat from "./call-api.js"

const app = express()

app.use('/meow', async (req, res, next) => {
    const urlA = req.url.split('/')
    
    let code = 200
    if(urlA && urlA[1]) code = urlA[1]

    const responseData = await getCat(code)
   
    const img = responseData.data
    res.writeHead(200, {
        'Content-Type': 'image/png',
        'Content-Length': img.length
    });
    res.end(img)

})

app.listen(config.PORT, () => {
    console.log(`Running at ${config.PORT}`);
})

