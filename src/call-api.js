import config from "./config.js"
import axios from "axios"

const getCat = async (httpCode) => {
    const urlRequest = `${config.URL}${httpCode}`
    const result = await axios.get(urlRequest, {
        responseType: 'arraybuffer'
    })
    return result
}

export default getCat