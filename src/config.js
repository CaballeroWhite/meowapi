import dotenv from 'dotenv'

dotenv.config()

const config = {
    URL: process.env.CAT_API,
    PORT: process.env.PORT
}

export default config